﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float force = 100.0f;
    public float jumpForce = 300.0f;
    public Text countText;
    public Text winText;
    
    bool jumpAvailable;
    Rigidbody rb;
    int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jumpAvailable = true;
        count = 0;
        winText.text = "";

        SetCountText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float jump = Input.GetAxis("Jump");

        rb.AddForce(force * horizontal * Time.deltaTime, 0, force * vertical * Time.deltaTime);

        if (Mathf.Approximately(jump, 1) && jumpAvailable)
        {
            rb.AddForce(0, jumpForce * Time.deltaTime, 0);
            jumpAvailable = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            jumpAvailable = true;
        }

        if (other.gameObject.CompareTag("Collectible"))
        {
            count++;
            other.gameObject.SetActive(false);
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 16)
        {
            winText.text = "You Win!";
        }
    }
}
